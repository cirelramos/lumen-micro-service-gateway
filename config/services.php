<?php

return [
    'authors' => [
        'base_uri' => 'http://'.env( 'SERVICE_AUTHOR_URI' ),
        'secret' => env( 'SERVICE_AUTHOR_SECRET' ),
    ],
    'books' => [
        'base_uri' => 'http://'.env( 'SERVICE_BOOK_URI' ),
        'secret' => env( 'SERVICE_BOOK_SECRET' ),
    ],
];
