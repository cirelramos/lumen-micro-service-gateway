<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAuthorsTable
 */
class CreateAuthorsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {

        Schema::create( 'authors', function ( Blueprint $table ) {

            $table->increments( 'id_author' );
            $table->string( 'name' );
            $table->string( 'gender' );
            $table->string( 'country' );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {

        Schema::dropIfExists( 'authors' );
    }
}
