<?php

namespace App\Services;

use App\Traits\ConsumesExternalService;
use Illuminate\Config\Repository;

/**
 * Class AuthorService
 * @package App\Services
 */
class AuthorService
{

    use ConsumesExternalService;

    /**
     * @var
     */
    public $baseUri;

    /**
     * @var Repository
     */
    public $secret;

    public function __construct()
    {

        $this->baseUri = config( 'services.authors.base_uri' );
        $this->secret  = config( 'services.authors.secret' );
    }

    public function index()
    {

        return $this->performRequest( 'GET', '/authors' );
    }

    /**
     * Create one author using the author service
     * @return string
     */
    public function store( $data )
    {

        return $this->performRequest( 'POST', '/authors', $data );
    }

    /**
     * Obtain one single author from the author service
     * @return string
     */
    public function show( $author )
    {

        return $this->performRequest( 'GET', "/authors/{$author}" );
    }

    /**
     * Update an instance of author using the author service
     * @return string
     */
    public function update( $data, $author )
    {

        return $this->performRequest( 'PUT', "/authors/{$author}", $data );
    }

    /**
     * Remove a single author using the author service
     * @return string
     */
    public function delete( $author )
    {

        return $this->performRequest( 'DELETE', "/authors/{$author}" );
    }
}
