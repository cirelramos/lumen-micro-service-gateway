<?php

namespace App\Services;

use App\Traits\ConsumesExternalService;

/**
 * Class AuthorService
 * @package App\Services
 */
class BookService
{
    use ConsumesExternalService;

    /**
     * @var
     */
    public $baseUri;

    /**
     * @var \Illuminate\Config\Repository
     */
    public $secret;

    public function __construct()
    {

        $this->baseUri = config( 'services.books.base_uri' );
        $this->secret  = config( 'services.books.secret' );
    }

    public function index()
    {
        return $this->performRequest('GET','/books');
    }
    /**
     * Create one book using the book service
     * @return string
     */
    public function store($data)
    {
        return $this->performRequest('POST', '/books', $data);
    }

    /**
     * Obtain one single book from the book service
     * @return string
     */
    public function show($book)
    {
        return $this->performRequest('GET', "/books/{$book}");
    }

    /**
     * Update an instance of book using the book service
     * @return string
     */
    public function update($data, $book)
    {
        return $this->performRequest('PUT', "/books/{$book}", $data);
    }

    /**
     * Remove a single book using the book service
     * @return string
     */
    public function delete($book)
    {
        return $this->performRequest('DELETE', "/books/{$book}");
    }

}
