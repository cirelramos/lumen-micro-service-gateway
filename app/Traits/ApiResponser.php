<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Laravel\Lumen\Http\ResponseFactory;

/**
 * Trait ApiResponser
 * @package App\Traits
 */
trait ApiResponser
{

    /**
     * @param array  $data
     * @param string $message
     * @param int    $code
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response|ResponseFactory
     */
    public function successResponse( $data = [], $message = '', $code = Response::HTTP_OK )
    {
        return response($data, $code)->header('Content-Type', 'application/json');
//        return response( $data, $code, $message )->header( 'Content-Type', 'application/json' );
        //        return response()->json( [ 'code' => $code, 'message' => $message, 'data' => $data, ], $code );

    }

    /**
     * @param     $data
     * @param int $code
     * @return JsonResponse
     */
    public function validResponse( $data, $code = Response::HTTP_OK ): JsonResponse
    {

        return response()->json( [ 'data' => $data ], $code );
    }

    /**
     * @param array  $data
     * @param string $message
     * @param int    $code
     * @return JsonResponse
     */
    public function errorResponse(
        $data = [],
        $message = '',
        $code = Response::HTTP_INTERNAL_SERVER_ERROR
    ): JsonResponse {

        return response()->json( [ 'code' => $code, 'message' => $message, 'data' => $data ], $code );

    }

    /**
     * @param $message
     * @param $code
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response|ResponseFactory
     */
    public function errorMessage( $message, $code )
    {

        return response( $message, $code )->header( 'Content-Type', 'application/json' );
    }
}
