<?php

namespace App\Traits;

use GuzzleHttp\Client;

/**
 * Trait consumesExternalService
 * @package App\Traits
 */
trait ConsumesExternalService
{

    /**
     * @param       $method
     * @param       $requestUrl
     * @param array $formParams
     * @param array $headers
     * @return mixed
     */
    public function performRequest( $method, $requestUrl, $formParams = [], $headers = [] )
    {

        $client        = new Client( [ 'base_uri' => $this->baseUri ] );

        if ( isset( $this->secret ) ) {
            $headers[ 'Authorization' ] = $this->secret;
        }

        $formAndHeader = [ 'form_params' => $formParams, 'headers' => $headers, ];

        $response = $client->request( $method, $requestUrl, $formAndHeader );

        return $response->getBody()->getContents();

    }

}
