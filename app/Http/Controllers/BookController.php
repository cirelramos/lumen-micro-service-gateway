<?php

namespace App\Http\Controllers;

use App\Services\BookService;
use App\Traits\ApiResponser;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

/**
 * Class bookController
 * @package App\Http\Controllers
 */
class BookController extends Controller
{

    use ApiResponser;

    /**
     * @var BookService
     */
    private $bookService;

    /**
     * Create a new controller instance.
     *
     * @param BookService $bookService
     */
    public function __construct(BookService $bookService)
    {

        $this->middleware( 'client.credentials' );
        $this->bookService = $bookService;
    }

    public function index()
    {
        return $this->successResponse($this->bookService->index());

    }

    /**
     * Create one new author
     * @return JsonResponse
     */
    public function store( Request $request )
    {

        return $this->successResponse( $this->bookService->store( $request->all(), Response::HTTP_CREATED ) );
    }

    /**
     * Obtains and show one author
     * @return JsonResponse
     */
    public function show( $author )
    {

        return $this->successResponse( $this->bookService->show( $author ) );
    }

    /**
     * Update an existing author
     * @return JsonResponse
     */
    public function update( Request $request, $author )
    {

        return $this->successResponse( $this->bookService->update( $request->all(), $author ) );
    }

    /**
     * Remove an existing author
     * @return JsonResponse
     */
    public function destroy( $author )
    {

        return $this->successResponse( $this->bookService->delete( $author ) );
    }

}
