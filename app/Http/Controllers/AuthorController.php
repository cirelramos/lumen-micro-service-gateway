<?php

namespace App\Http\Controllers;

use App\Services\AuthorService;
use App\Traits\ApiResponser;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class AuthorController
 * @package App\Http\Controllers
 */
class AuthorController extends Controller
{

    use ApiResponser;

    /**
     * @var AuthorService
     */
    private $authorService;

    /**
     * Create a new controller instance.
     *
     * @param AuthorService $authorService
     */
    public function __construct( AuthorService $authorService )
    {

        $this->middleware( 'client.credentials' );
        $this->authorService = $authorService;
    }

    public function index()
    {

        return $this->successResponse( $this->authorService->index() );

    }

    /**
     * Create one new author
     * @return JsonResponse
     */
    public function store( Request $request )
    {

        return $this->successResponse( $this->authorService->store( $request->all(), Response::HTTP_CREATED ) );
    }

    /**
     * Obtains and show one author
     * @return JsonResponse
     */
    public function show( $author )
    {

        return $this->successResponse( $this->authorService->show( $author ) );
    }

    /**
     * Update an existing author
     * @return JsonResponse
     */
    public function update( Request $request, $author )
    {

        return $this->successResponse( $this->authorService->update( $request->all(), $author ) );
    }

    /**
     * Remove an existing author
     * @return JsonResponse
     */
    public function destroy( $author )
    {

        return $this->successResponse( $this->authorService->delete( $author ) );
    }

}
