<?php

namespace App\Http;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Author
 * @package App
 */
class Author extends Model
{

    protected $primaryKey = 'id_author';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'gender',
        'country',
    ];
}
